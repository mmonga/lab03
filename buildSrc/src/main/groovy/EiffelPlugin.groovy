/*
  This is a very simplistic Eiffel plugin:
  - all the Eiffel dependencies complexity is hidden in the `.ecf` file,
    which is assumed as already existing;
  - it relies on the correct installation of the compiler (ec) and the
    finalizer (finish_freezing)
*/

import org.gradle.api.Plugin
import org.gradle.api.Project

class EiffelPlugin implements Plugin<Project> {
    void apply(Project project) {
        project.extensions.create("eiffel", EiffelPluginExtension)
        project.task('melt') << {
          if (!project.buildDir.exists()) {
            project.buildDir.mkdirs()
          }
            project.exec {
              environment 'ISE_EIFFEL', project.eiffel.ise_eiffel
              environment 'ISE_PLATFORM', project.eiffel.ise_platform
              workingDir 'src'
              commandLine project.eiffel.ebin + '/ec', '-project_path', "$project.buildDir",
              '-batch', '-config',  project.eiffel.targetcfg, '-finalize', '-c_compile'
            }
        }

        project.task('finish_freezing') << {
            project.exec {
              environment 'ISE_EIFFEL', project.eiffel.ise_eiffel
              environment 'ISE_PLATFORM', project.eiffel.ise_platform
              workingDir "$project.buildDir" + '/EIFGENs/' + project.eiffel.target + '/W_code'
              commandLine project.eiffel.ebin + '/finish_freezing'
            }
            project.copy {
              from "$project.buildDir" + '/EIFGENs/' + project.eiffel.target + '/W_code'
              into "$project.buildDir"
              include "${project.eiffel.target}"
            }
        }

        project.task('cleanEiffel') << {
          project.buildDir.deleteDir()
        }

        project.tasks.getByName('finish_freezing').dependsOn(project.tasks.getByName('melt'))
    }
}

class EiffelPluginExtension {
  def String ise_eiffel = System.getenv()['ISE_EIFFEL'] ? System.getenv()['ISE_EIFFEL'] : '/opt/Eiffel_16.05'
  def String ise_platform = System.getenv()['ISE_PLATFORM'] ? System.getenv()['ISE_PLATFORM'] : 'linux-x86-64'
  def String ebin = ise_eiffel + '/studio/spec/' + ise_platform + '/bin'
  def String targetcfg
  def String target
}
