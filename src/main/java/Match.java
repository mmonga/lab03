import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Random;

public class Match {
    private Logger logger = LoggerFactory.getLogger(Match.class);
    private Game g = new Game();

    void doRoll(int pins){
        logger.info("Rolling: " + pins);
        g.roll(pins);
        logger.info("Score: " + g.score());
    }

    public static void main(String[] args) {
        Match m = new Match();
        Random r = new Random();
        for (int frame=1; frame <= 10; frame++) {
            int[] rolls = new int[3];
            rolls[0] = r.nextInt(11);
            m.doRoll(rolls[0]);
            if (rolls[0] < 10 || (frame == 10 && rolls[0] == 10)) {
                rolls[1] = r.nextInt(11);
                m.doRoll(rolls[1]);
            }
            if (frame == 10 && (rolls[0] == 10 || rolls[0] + rolls[1] == 10)) {
                rolls[2] = r.nextInt(11);
                m.doRoll(rolls[2]);
            }
        }
    }
}
